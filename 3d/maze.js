// get config
const CONTRACT_NAME =
  window.location.host === "corgis3d.com"
    ? "corgis3d.yifang.near"
    : "corgis.nft-corgis.testnet";

function getConfig(env) {
  switch (env) {
    case "mainnet":
      return {
        networkId: "mainnet",
        nodeUrl: "https://rpc.mainnet.near.org",
        contractName: CONTRACT_NAME,
        walletUrl: "https://wallet.near.org",
        helperUrl: "https://helper.mainnet.near.org",
      };
    case "production":
    case "development":
    case "testnet":
      return {
        networkId: "default",
        nodeUrl: "https://rpc.testnet.near.org",
        contractName: CONTRACT_NAME,
        walletUrl: "https://wallet.testnet.near.org",
        helperUrl: "https://helper.testnet.near.org",
      };
    case "betanet":
      return {
        networkId: "betanet",
        nodeUrl: "https://rpc.betanet.near.org",
        contractName: CONTRACT_NAME,
        walletUrl: "https://wallet.betanet.near.org",
        helperUrl: "https://helper.betanet.near.org",
      };
    case "test":
    case "ci":
      return {
        networkId: "shared-test",
        nodeUrl: "https://rpc.ci-testnet.near.org",
        contractName: CONTRACT_NAME,
        masterAccount: "test.near",
      };
    case "ci-betanet":
      return {
        networkId: "shared-test-staging",
        nodeUrl: "https://rpc.ci-betanet.near.org",
        contractName: CONTRACT_NAME,
        masterAccount: "test.near",
      };
    default:
      throw Error(
        `Unconfigured environment '${env}'. Can be configured in src/config.js.`
      );
  }
}

// Initializing contract
const InitContract = async () => {
  let configName =
    window.location.host === "corgis3d.com" ? "mainnet" : "testnet";
  const nearConfig = getConfig(configName);

  // Initializing connection to the NEAR
  const near = await nearApi.connect({
    deps: {
      keyStore: new nearApi.keyStores.BrowserLocalStorageKeyStore(),
    },
    ...nearConfig,
  });
  // Needed to access wallet
  const walletConnection = new nearApi.WalletConnection(near);

  // Initializing our contract APIs by contract name and configuration.
  const contract = await new nearApi.Contract(
    walletConnection.account(),
    nearConfig.contractName,
    {
      // View methods are read only. They don't modify the state, but usually return some value.
      viewMethods: [],
      // Change methods can modify the state. But you don't receive the returned value when called.
      changeMethods: ["new_maze_game", "finish_maze_game"],
      // Sender is the account ID to initialize transactions.
      sender: walletConnection.getAccountId(),
    }
  );
  return contract;
};

const startNewMazeGame = async () => {
  const fruitMap = {
    0: "apple",
    1: "avocado",
    2: "banana",
    3: "cucumber",
    4: "lemon",
    5: "lime",
    6: "orange",
  };
  try {
    const contract = await InitContract();
    localStorage.removeItem("fruit");
    let res = await contract.new_maze_game();
    let fruits = res.fruit;
    let final = fruits.map((f) => ({
      type: fruitMap[f.kind],
      x: f.x,
      y: f.y,
    }));
    localStorage.setItem("fruit", JSON.stringify(final));
    console.log("fruit is ready");
  } catch (e) {
    console.log(e);
  }
};

const finishMazeGame = async () => {
  const fruitMap = {
    apple: 0,
    avocado: 1,
    banana: 2,
    cucumber: 3,
    lemon: 4,
    lime: 5,
    orange: 6,
  };
  try {
    const contract = await InitContract();
    let eatF = JSON.parse(localStorage.getItem("eat"));
    let eat = eatF.map((e) => ({
      kind: fruitMap[e.type],
      x: e.x,
      y: e.y,
    }));
    await contract.finish_maze_game({ eat });
    console.log("maze game is over");
  } catch (e) {
    console.log(e);
  }
};

window.startNewMazeGame = startNewMazeGame;
window.finishMazeGame = finishMazeGame;
