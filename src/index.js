import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import getConfig from "./config.js";
import * as nearlib from "near-api-js";

import App from "./App";
import NearContextProvider from "./context/NearContext";
import ContractContextProvider from "./hooks/contract";

// Initializing contract
async function InitContract() {
  let configName = process.env.REACT_APP_NODE_ENV || "testnet";
  configName = window.location.host === "corgis3d.com" ? configName : "testnet";
  const nearConfig = getConfig(configName);

  // Initializing connection to the NEAR
  let near;
  try {
    near = await nearlib.connect({
      deps: {
        keyStore: new nearlib.keyStores.BrowserLocalStorageKeyStore(),
      },
      ...nearConfig,
    });
  } catch (e) {
    console.log(e);
  }

  // Needed to access wallet
  let walletConnection;
  try {
    walletConnection = new nearlib.WalletConnection(near);
  } catch (e) {
    console.log(e);
  }

  // Load in account data
  let currentUser;
  if (walletConnection.getAccountId()) {
    currentUser = {
      accountId: walletConnection.getAccountId(),
      balance: (await walletConnection.account().state()).amount,
    };
  }

  // Initializing our contract APIs by contract name and configuration.
  let contract;
  try {
    contract = await new nearlib.Contract(
      walletConnection.account(),
      nearConfig.contractName,
      {
        // View methods are read only. They don't modify the state, but usually return some value.
        viewMethods: [
          "get_corgi",
          "get_corgis_by_owner",
          "display_global_corgis",
          "get_token_owner",
          "account_fruit",
        ],
        // Change methods can modify the state. But you don't receive the returned value when called.
        changeMethods: [
          "transfer_with_message",
          "create_corgi",
          "transfer_from_with_message",
          "delete_corgi",
          "sell_corgi",
          "buy_corgi",
          "new_maze_game",
        ],
        // Sender is the account ID to initialize transactions.
        sender: walletConnection.getAccountId(),
      }
    );
  } catch (e) {
    console.log(e);
  }

  if (contract) {
    return { contract, currentUser, nearConfig, walletConnection, near };
  }
  return "near is down";
}

window.nearInitPromise = InitContract()
  .then(({ contract, currentUser, nearConfig, walletConnection, near }) => {
    const app = (
      <NearContextProvider
        currentUser={currentUser}
        nearConfig={nearConfig}
        wallet={walletConnection}
        near={near}
      >
        <ContractContextProvider Contract={contract}>
          <BrowserRouter basename={process.env.REACT_APP_PUBLIC_URL}>
            <App />
          </BrowserRouter>
        </ContractContextProvider>
      </NearContextProvider>
    );

    ReactDOM.render(app, document.getElementById("root"));
  })
  .catch((e) => {
    const app = (
      <BrowserRouter basename={process.env.REACT_APP_PUBLIC_URL}>
        <App error={e} />
      </BrowserRouter>
    );

    ReactDOM.render(app, document.getElementById("root"));
  });
