import React from "react";
import { Route, Switch } from "react-router-dom";

import Dash from "./component/Dash/Dash";
import Market from "./component/Market/Market";

import Account from "./component/Account/Account";
import Profile from "./component/Profile/Profile";

import Generation from "./component/Generation/Generation";

import SinglePage from "./component/SinglePage/SinglePage";
import SharePage from "./component/SharePage/SharePage";

import Header from "./component/Header/Header";
import Footer from "./component/Footer/Footer";

const App = ({ error }) => {
  if (error) {
    return (
      <div className="container">
        <h1>
          NEAR is happening to have some issues, corgis 3D cannot successfully
          connect to NEAR protocol
        </h1>
        <p>error message: {JSON.stringify(error)}</p>
      </div>
    );
  }
  return (
    <div>
      <Header />
      <div className="container">
        <Switch>
          <Route exact path="/" component={Dash} />
          <Route exact path="/generation" component={Generation} />
          <Route exact path="/account" component={Account} />
          <Route exact path="/profile" component={Profile} />
          <Route exact path="/corgi/:id" component={SinglePage} />
          <Route exact path="/share/:id" component={SharePage} />
          <Route exact path="/market" component={Market} />
          <Route
            render={() => (
              <h1>
                Not found This page. Please go back to continue or you can
                contact us about the issue.
              </h1>
            )}
          />
        </Switch>
      </div>
      <Footer />
      <style>{`
        body {
          text-align: center;
          font-family: 'Nunito', sans-serif;
          position: relative;
          min-height: calc(100vh - 80px);
          background: linear-gradient(218.23deg, #E4FFC9 -3.26%, #ACE471 101.84%);
          color: #1c270e;
          max-width: 1400px;
        }
        
        button {
          all: unset;
          outline:none;
        }
        
        input {
          all:unset;
          padding: 5px;
        }

        .container {
          margin: 10px 5%;
          border-radius: 10px;
          background: linear-gradient(212.85deg, #9BE94A 0%, #E4FFC9 100%);
        }
      `}</style>
    </div>
  );
};

export default App;
