import React, { useState, useCallback } from "react";

export const NearContext = React.createContext({
  user: null,
  nearContract: null,
  signIn: () => {},
  signOut: () => {},
  isLoading: false,
});

const NearContextProvider = ({
  currentUser,
  nearConfig,
  wallet,
  near,
  children,
}) => {
  const user = useState(currentUser)[0];
  const nearContent = useState(near)[0];
  const [isLoading, setLoading] = useState(false);

  const signIn = useCallback(() => {
    wallet.requestSignIn(nearConfig.contractName, "NEAR Corgi");
    window.location = process.env.REACT_APP_PUBLIC_URL || "/";
  }, [wallet, nearConfig]);

  const signOut = useCallback(() => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
    setTimeout(() => setLoading(false), 2000);
    setLoading(true);
  }, [wallet]);

  return (
    <NearContext.Provider
      value={{
        user,
        signIn,
        signOut,
        isLoading,
        nearContent,
      }}
    >
      {children}
    </NearContext.Provider>
  );
};

export default NearContextProvider;
