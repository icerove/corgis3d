import React from "react";

import Corgi from "../../CorgiCard/Corgi/Corgi";
import { Dialogue } from "../../CorgiCard/Card";

const AccountCard = ({ corgi }) => (
  <div className="creation">
    <div className="corgiboard">
      <div
        style={{
          backgroundColor: corgi.background_color,
          padding: "10px",
          display: "inline-block",
          borderRadius: "10px",
          height: "240px",
          marginBottom: "1px",
          width: "280px",
        }}
      >
        <Dialogue quote={corgi.quote} color={corgi.color} />
        <Corgi color={corgi.color} sausage={corgi.sausage} />
      </div>
    </div>
    <p className="dogname">Name: {corgi.name}</p>
    <p className="address">Rate: {corgi.rate}</p>
    <style>{`
            .creation {
              margin: 1%;
              display: inline-block;
              border-radius: 10px;
              border: 2px solid #4bb512;
              background: #fff;
              padding: 10px;
              width: 300px;
          }
          
          .corgiboard {
              border-radius: 10px;
          }
                
                .dogname {
                    color: #1c270e;
                    text-align: left;
                    font-size: 1em;
                    margin-left: 1%;
                    margin-top: 1%;
                    margin-bottom: 0;
                    display: block;
                }
                
                .address {
                    text-align: left;
                    font-size: 0.7em;
                    margin-left: 1%;
                    font-weight: lighter;
                    display: block;
                    color: #1c270e;
                }
                
                .orange {
                    color: orange;
                }
            `}</style>
  </div>
);

export default AccountCard;
