import React from "react";

const Button = ({ action, description, disabled = false }) => {
  return (
    <button className="Button" onClick={action} disabled={disabled}>
      {description}
      <style>{`
        .Button {
          background: #F3F64D;
          box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
          border-radius: 5px;
          color: #000;
          font-weight: 600;
          margin-right: 10px;
          padding: 5px 30px;
          letter-spacing: 0;
          white-space: nowrap;
          cursor: pointer;
          margin-left: 3px;
        }
        
        .Button:disabled {
          background: #cfcfcf;
          color: #888888;
        }
        
        @media all and (max-width: 451px) {
          .Button {
            padding: 8px 3px;
          }
        }
        `}</style>
    </button>
  );
};
export default Button;
