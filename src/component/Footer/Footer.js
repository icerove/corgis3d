import React from "react";

import Logo from "../../assets/images/corgi-circle.png";

const Footer = () => (
  <div className="footer">
    <div className="left">
      <img src={Logo} alt="" style={{ width: "60px", height: "60px" }} />
      <div className="left-text">
        <p>3D Corgis NFT </p>
        <p>built on NEAR Protocol. </p>
        <p>
          Learn more at{" "}
          <a
            href="https://near.org/"
            target="_blank"
            rel="noopener noreferrer"
            className="blue"
          >
            near.org
          </a>
        </p>
      </div>
    </div>
    <div className="right">
      <p>© {new Date().getFullYear()} </p>
      <p>All Rights Reserved.</p>
    </div>
    <style>{`
      .footer {
        display: flex;
        justify-content: space-between;
        padding: 1%;
        font-size: 0.7rem;
        font-weight: 200;
        width: 90%;
        max-width: 1400px;
        margin:auto;
    }
    
    .footer p {
        margin:0;
    }
    
    .left-text {
        padding: 10px;
    }
    
    .left {
        text-align: start;
        margin-left: 15px;
        display: inline-flex;
        justify-content: flex-start;
    }
    
    .right {
        display: block;
        text-align: end;
        margin-right: 15px;
        padding-top: 10px;
    }
    
    .blue {
        color: lightblue;
    }
    
    .black {
        color: black;
    }
    `}</style>
  </div>
);

export default Footer;
