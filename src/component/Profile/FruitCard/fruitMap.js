import Apple from "../../../assets/images/apple.png";
import Avocado from "../../../assets/images/avocado.png";
import Cucumber from "../../../assets/images/cucumber.png";
import Banana from "../../../assets/images/banana.png";
import Lemon from "../../../assets/images/lemon.png";
import Lime from "../../../assets/images/lime.png";
import Orange from "../../../assets/images/orange.png";

export const fruitMap = {
  Apple,
  Avocado,
  Cucumber,
  Banana,
  Lemon,
  Lime,
  Orange,
};
