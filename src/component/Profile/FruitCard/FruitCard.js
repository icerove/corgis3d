import React from "react";

const fruitCard = ({ fruitName, fruitCount, fruitImage }) => {
  return (
    <div className="fruit-diamond">
      <img src={fruitImage} alt={fruitName} className="fruit-image" />
      <p>
        {fruitName}: {fruitCount}
      </p>
      <style>{`
      .fruit-diamond {
        width: 220px;
        border: 2px solid #7FDBFF;
        border-radius: 10px;
        text-align: center;
        margin: 1%;
        padding-top:3px;
      } 
      
      
      .fruit-image {
        border: 2px solid #7FDBFF;
        border-radius: 50%;
        padding: 5px;
      }

      @media all and (max-width: 450px) {
        .fruit-diamond {
          width: 90%;
          margin:auto;
        }
      }
      `}</style>
    </div>
  );
};

export default fruitCard;
