import React, { useContext, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { NearContext } from "../../context/NearContext";
import { ContractContext, fruitList } from "../../hooks/contract";

import Spinner from "../utils/Spinner";
import { fruitMap } from "./FruitCard/fruitMap";
import FruitCard from "./FruitCard/FruitCard";

const Profile = () => {
  const nearContext = useContext(NearContext);
  const useContract = useContext(ContractContext);
  const { loading, error, fruit, getFruits } = useContract;

  useEffect(() => {
    if (nearContext.user) {
      getFruits(nearContext.user.accountId);
    }
  }, [getFruits, nearContext]);

  if (!nearContext.user) {
    return <Redirect to="/" />;
  }
  if (!fruit || loading) {
    return <Spinner />;
  }

  return (
    <div className="profile">
      <h1>Collections</h1>
      {error && <p>{error}</p>}
      <div>
        <h3>Fruit Collections:</h3>
        <p>get fruit collections from Corgi Farm</p>
        {fruit.count && (
          <div className="fruit">
            {fruit.count.map((f, i) => (
              <FruitCard
                fruitImage={fruitMap[fruitList[i]]}
                fruitCount={f}
                fruitName={fruitList[i]}
              />
            ))}
          </div>
        )}
      </div>
      <style>{`
        .profile {
          padding-bottom: 20px;
        }
        .fruit {
          padding: 1%;
          display: flex;
          flex-wrap: wrap;
          width: 100%;
        }
      `}</style>
    </div>
  );
};
export default Profile;
