import React from "react";
import Corgi from "./Corgi/Corgi";

export const BigCard = ({ backgroundColor, color, quote, sausage }) => {
  return (
    <div
      style={{
        backgroundColor: backgroundColor,
        borderRadius: "10px",
        padding: "10% 0",
        width: "100%",
        height: "100%",
        marginLeft: "3px",
      }}
    >
      <BigDialogue quote={quote} color={color} />
      <BigCorgi color={color} sausage={sausage} />
    </div>
  );
};

const BigCorgi = ({ color, sausage }) => {
  return (
    <div
      style={{
        width: "90%",
        margin: "auto",
        maxWidth: "700px",
      }}
    >
      <Corgi color={color} sausage={sausage} />
    </div>
  );
};

const BigDialogue = ({ quote, color }) => {
  return (
    <div
      style={{
        width: "90%",
        padding: "8px",
        wordWrap: "breakWord",
        backgroundColor: "white",
        opacity: "0.7",
        borderRadius: "20px",
        marginLeft: "10px",
        marginBottom: "30px",
      }}
    >
      <p style={{ color: color, filter: "brightness(50%)", margin: "0" }}>
        <i className="fa fa-quote-left"></i> {quote}
      </p>
    </div>
  );
};

export const SmallCard = ({ backgroundColor, color, quote, sausage }) => {
  return (
    <div
      style={{
        backgroundColor: backgroundColor,
        borderRadius: "10px",
        padding: "20px",
        display: "inline-block",
        height: "220px",
      }}
    >
      <Dialogue quote={quote} color={color} />
      <Corgi color={color} sausage={sausage} />
    </div>
  );
};

export const Dialogue = ({ quote, color }) => {
  return (
    <div
      style={{
        fontSize: "0.8em",
        width: "94%",
        padding: "5px",
        wordWrap: "break-word",
        backgroundColor: "white",
        opacity: "0.7",
        borderRadius: "5px",
      }}
    >
      <p style={{ color: color, filter: "brightness(50%)", margin: "0" }}>
        <i className="fa fa-quote-left"></i> {quote}
      </p>
    </div>
  );
};
