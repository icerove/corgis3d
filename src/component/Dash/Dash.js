import React, { useContext, useEffect } from "react";

import { NearContext } from "../../context/NearContext";
import { ContractContext } from "../../hooks/contract";

import Poster from "./Poster/Poster";
import ShowCase from "./ShowCase/ShowCase";

const Dash = () => {
  const nearContext = useContext(NearContext);
  const useContract = useContext(ContractContext);
  const { getDisplayCorgis, displayCorgis, buyCorgi, buying } = useContract;

  useEffect(() => getDisplayCorgis(), [getDisplayCorgis]);

  const signIn = () => {
    nearContext.signIn();
  };
  let corgis = displayCorgis
    ? displayCorgis.filter((corgi) => corgi.selling === true)
    : [];
  let topCorgis = corgis
    .sort((a, b) => Number(b.selling_price) - Number(a.selling_price))
    .slice(0, 4);
  return (
    <div className="Dash">
      <Poster
        requestSignIn={signIn}
        isLoading={nearContext.isLoading}
        user={nearContext.user}
        sellingCount={corgis.length}
      />
      {topCorgis.length > 0 && (
        <ShowCase
          topCorgis={topCorgis}
          buyCorgi={buyCorgi}
          buying={buying}
          dash={true}
        />
      )}
      <style>{`
            .Dash {
                width: 100%;
                margin: auto;
                text-align: center;
            }
        `}</style>
    </div>
  );
};

export default Dash;
