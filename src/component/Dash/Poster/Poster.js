import React from "react";
import { NavLink } from "react-router-dom";

import Button from "../../utils/Button";
import Spinner from "../../utils/Spinner";

import corgiFull from "../../../assets/images/corgi-full.png";

const Poster = ({ requestSignIn, isLoading, user, sellingCount }) => {
  let showButton;
  if (isLoading) {
    return <Spinner />;
  }
  if (!user) {
    showButton = (
      <Button description="Login with NEAR" action={requestSignIn} />
    );
  } else {
    showButton = (
      <div className="poster-button">
        <div className="show">{user.accountId}</div>
        <NavLink exact to="/market">
          <div className="market">Discover Markets ({sellingCount})</div>
        </NavLink>
      </div>
    );
  }
  return (
    <div className="wrapper">
      <div className="backup">
        <div className="textPoster">
          <p className="text1">Corgis 3D is playful NFT </p>
          <p className="text1">Explore 3D Corgis world today</p>
          <p className="text2">create, collect, upgrade and play</p>
          {showButton}
        </div>
        <div className="imagePoster">
          <img src={corgiFull} alt="" style={{ height: "350px" }} />
        </div>
      </div>
      <style>{`
    .wrapper{
        width: 90%;
        margin:auto;
        max-width: 1100px;
        display: flex;
        flex-direction: column;
      }
      
      .backup {
        display: flex;
        flex-direction: row;
        padding: 2%;
        justify-items: center;
        align-items: center;
        margin-top: 1%;
      }
      
      .textPoster {
        text-align: left;
        height: 100%;
      }
      
      .text1 {
        font-weight: 600;
        font-size: 2em;
        color: #24272a;
        letter-spacing: 0;
      }
      
      .text2 {
        font-size: 1em;
        font-weight: 100;
        color: #24272a;
        letter-spacing: 0;
      }
      
      .imagePoster {
        height: 100%;
      }
      
      .show {
        border-radius: 5px;
        border: 2px solid #F3F64D;
        background-color: #fffdf7;
        padding: 5px 20px;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
       
      }

      .market {
        border-radius: 5px;
        border: 2px solid #F3F64D;
        background-color: #F3F64D;
        padding: 5px 20px;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        
      }

      .poster-button {
        display: flex;
        flex-direction: row;
        text-align: center;
        justify-content: space-between;
      }
      
      @media all and (max-width: 1000px) {
        .wrapper {
          width: 70%;
        }
      
        .backup {
          display: grid;
          grid-template-areas: "text"
                              "image";
        }
      
        .imagePoster {
          margin-top: 2%;
          grid-area: image;
        }
      }
      
      @media all and (max-width: 450px) {
        .wrapper {
          width: 90%;
        }
      
        .backup {
          display: grid;
          grid-template-areas: "text"
                              "image";
        }
      
        .imagePoster {
          display:none;
        }

        .market {
          padding: 0;
        }
      }
            
  `}</style>
    </div>
  );
};

export default Poster;
