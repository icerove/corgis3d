import React from "react";
import { Link } from "react-router-dom";

import DashCard from "./DashCard/DashCard";

const ShowCase = ({ topCorgis, buyCorgi, buying }) => {
  let TCorgis = topCorgis.map((corgi) => {
    return (
      <Link to={`/share/${corgi.id}`} key={corgi.id}>
        <DashCard corgi={corgi} buyCorgi={buyCorgi} buying={buying} />
      </Link>
    );
  });

  return (
    <div className="corgis-display">
      {TCorgis}
      <style>{`
            .corgis-display {
                width: 100%;
                margin: 25px 1%;
                display: flex;
                flex-wrap: wrap;
                justify-content:space-evenly;
            }
        `}</style>
    </div>
  );
};

export default ShowCase;
