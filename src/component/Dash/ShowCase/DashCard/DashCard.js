import React from "react";

import Corgi from "../../../CorgiCard/Corgi/Corgi";
import { Dialogue } from "../../../CorgiCard/Card";
import { Redirect } from "react-router-dom";
import Button from "../../../utils/Button";
const DashCard = ({ corgi, buyCorgi, buying }) => {
  const buy = () => {
    buyCorgi(corgi.id, corgi.selling_price);
  };
  if (buying === "finish") {
    return <Redirect push to="/account" />;
  }
  return (
    <div className="creation">
      <div className="corgiboard">
        <div
          style={{
            backgroundColor: corgi.background_color,
            padding: "10px",
            display: "inline-block",
            borderRadius: "10px",
            height: "240px",
            marginBottom: "1px",
            width: "200px",
          }}
        >
          <Dialogue quote={corgi.quote} color={corgi.color} />
          <Corgi color={corgi.color} sausage={corgi.sausage} />
        </div>
      </div>
      <div className="dogname">
        <p>
          Name: <strong>{corgi.name}</strong>
        </p>
        <p>
          Rarity: <strong>{corgi.rate}</strong>
        </p>
        <p>
          Sausage: <strong>{corgi.sausage}</strong>
        </p>
        <p className="address">
          Price:
          <strong>
            <span className="orange">{corgi.selling_price} Ⓝ </span>
          </strong>{" "}
          <Button description="Buy Now" action={buy} />
        </p>
      </div>

      <style>{`
            .creation {
                margin: 1%;
                display: inline-block;
                border-radius: 10px;
                border: 2px solid #4bb512;
                background: #fff;
                padding: 10px;
                width: 220px;
            }
            
            .corgiboard {
                border-radius: 10px;
            }
            
            .dogname {
                text-align: left;
                font-size: 0.8em;
                color: #1c270e;
            }
            
            .address {
                font-weight: lighter;
            }
            
            .orange {
                color: #4bb512;
            }
            
            .blue {
                color: lightblue;
            }
        `}</style>
    </div>
  );
};

export default DashCard;
