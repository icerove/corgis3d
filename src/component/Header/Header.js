import React, { useContext, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { utils } from "near-api-js";
import { NearContext } from "../../context/NearContext";
import { ContractContext } from "../../hooks/contract";

import Nav from "./Nav/Nav";

import logo from "../../assets/images/logo.png";
import Spinner from "../utils/Spinner";
import Button from "../utils/Button";

const Header = () => {
  const nearContext = useContext(NearContext);
  const useContract = useContext(ContractContext);
  const { getCorgisList, corgis } = useContract;
  useEffect(() => {
    if (nearContext.user) {
      getCorgisList(nearContext.user.accountId);
    }
  }, [getCorgisList, nearContext]);

  const signIn = () => {
    nearContext.signIn();
  };
  const signOut = () => {
    nearContext.signOut();
  };

  if (nearContext.isLoading) {
    return <Spinner />;
  }
  let show;
  if (nearContext.user) {
    show = (
      <div className="header">
        <NavLink exact to="/">
          <img
            src={logo}
            style={{
              minWidth: "80px",
              width: "100%",
              maxWidth: "150px",
              height: "50px",
            }}
            alt=""
          />
        </NavLink>
        <Nav
          accountName={nearContext.user.accountId}
          number={corgis ? corgis.length : "..."}
          requestSignOut={signOut}
          balance={utils.format.formatNearAmount(nearContext.user.balance, 5)}
        />
      </div>
    );
  } else {
    show = (
      <div className="header">
        <NavLink exact to="/">
          <img
            src={logo}
            style={{
              minWidth: "80px",
              width: "100%",
              maxWidth: "150px",
              height: "50px",
            }}
            alt=""
          />
        </NavLink>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            margin: "auto",
            marginRight: "30px",
          }}
        >
          <NavLink
            exact
            to="/market"
            className="nav"
            activeClassName="nav-active"
          >
            <div>Markets</div>
          </NavLink>
          <Button description="Get Started" action={signIn} />
        </div>
      </div>
    );
  }
  return (
    <div>
      {show}
      <style>{`
        .header {
            margin:auto;
            display: flex;
            justify-content: space-between;
            width: 90%;
            max-width: 1400px;
        }

        .nav {
          text-decoration: none;
          width: 100%;
          text-align: center;
          outline: none;
          display: block;
          font-size: 1.1em;
          color: #24272a;
          padding: 5px 10px;
          margin: 0 2px;
          border-radius: 10px;
          background: radial-gradient(circle, rgba(219, 255, 80, 0.733) 35%, rgba(146, 255, 0, 0.565) 100%);
          box-shadow: 0 0 2px 2px rgb(0 0 0 / 20%);
          transition: all 200ms ease-out;
        }

        .nav-active {
          background: radial-gradient(circle, rgba(146, 255, 0, 0.565) 100%, rgba(219, 255, 80, 0.733) 35%);
          box-shadow: 0 0 4px 4px rgb(0 0 0 / 40%);
        }
        
        @media all and (max-width: 751px) {
            .header{
                width: 90%;
                margin: auto;
            }
        }
        
        @media all and (max-width: 376px) {
            .header{
                width: 90%;
                margin: 1% auto;
                flex-direction: column;
            }
        }    
    `}</style>
    </div>
  );
};
export default Header;
