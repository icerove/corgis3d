import React from "react";
import { NavLink } from "react-router-dom";
import { GrMoney } from "react-icons/gr";
import { IoMdLogOut } from "react-icons/io";

const Nav = ({ number, accountName, requestSignOut, balance }) => {
  return (
    <div className="wrap">
      <div className="account">
        <div className="nav-line">
          <NavLink to="/account" className="nav" activeClassName="nav-active">
            <div>Corgis({number})</div>
          </NavLink>
          <NavLink to="/profile" className="nav" activeClassName="nav-active">
            <div>Collections</div>
          </NavLink>
          <NavLink
            to="/generation"
            className="nav"
            activeClassName="nav-active"
          >
            <div>Factory</div>
          </NavLink>
          <NavLink
            exact
            to="/market"
            className="nav"
            activeClassName="nav-active"
          >
            <div>Markets</div>
          </NavLink>
        </div>
        <Card
          accountName={accountName}
          requestSignOut={requestSignOut}
          balance={balance}
        />
      </div>
      <style>{`
        .wrap {
          margin: auto;
          margin-right:0;
          display: inline-flex;
          font-size: 1em;
        }
        
        .account{
          display: inline-flex;
          flex-direction: row;
          flex-wrap: wrap;
        }
        
        .menutop {
          border-radius: 5px;
          border: 2px solid #F3F64D;
          background-color: #fffdf7;
          padding: 5px 30px;
          box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
          cursor: grabbing;
        }
        
        .dropdown {
          position: relative;
          display: inline-block;
          margin: 0 15px;
        }
        
        .dropdown-content {
          display: none;
          position: absolute;
          background-color: #fdfcfc;
          width: 100%;
          z-index: 1;
          box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
          border-radius: 0 0 5px 5px;
        }
        
        .dropdown:hover .dropdown-content {
          display: block;
        }
        
        .menutop:hover .menutop {
            border: none;
            border-radius: 5px 5px 0 0;
        }
        
        li.active {
          background: #dddddd;
        }
        
        .orange {
          color: #fbb040;
          display: inline;
        }
        
        @media all and (max-width: 1200px) {
          .wrap{
            font-size: 0.8em;
          }
          .menutop {
            padding: 5px;
          }
        }

        .nav {
          text-decoration: none;
          width: 100%;
          text-align: center;
          outline: none;
          display: block;
          font-size: 1.1em;
          color: #24272a;
          padding: 5px 10px;
          margin: 0 2px;
          border-radius: 10px;
          background: radial-gradient(circle, rgba(219, 255, 80, 0.733) 35%, rgba(146, 255, 0, 0.565) 100%);
          box-shadow: 0 0 2px 2px rgb(0 0 0 / 20%);
          transition: all 200ms ease-out;
        }

        .nav-active {
          background: radial-gradient(circle, rgba(146, 255, 0, 0.565) 100%, rgba(219, 255, 80, 0.733) 35%);
          box-shadow: 0 0 4px 4px rgb(0 0 0 / 40%);
        }

        .nav-line {
          display: flex;
          flex-direction: row;
          margin:auto;
        }
        
        @media all and (max-width: 450px) {
          .wrap{
            font-size: 0.6em;
          }
          .account{
            flex-direction: row;
          }
          .menutop {
            padding: 8px 3px;
          }
          .dropdown{
            margin: 15px;
            width: 100%;
          } 
        }
      `}</style>
    </div>
  );
};

const Card = ({ accountName, requestSignOut, balance }) => {
  let style = {
    textDecoration: "none",
    display: "block",
    padding: "auto",
    color: "#2b90e3",
    margin: "2%  auto",
  };
  return (
    <div className="dropdown">
      <button className="menutop">{accountName}▾</button>
      <div className="dropdown-content">
        <ul
          style={{
            textAlign: "center",
            padding: "2px",
            marginBottom: "0",
            marginTop: "0",
          }}
        >
          <li style={style}>
            <GrMoney style={{ marginTop: "5px" }} /> {balance}
          </li>
          <li style={{ ...style, cursor: "pointer" }}>
            <button onClick={requestSignOut}>
              <IoMdLogOut style={{ marginTop: "5px" }} /> Sign Out
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Nav;
